﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour {

	public Vector3 offset;
	public int life;
	GameManager gameManager;
	void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

	 private void OnCollisionEnter2D(Collision2D other)
	{
		if(other.gameObject.CompareTag("Ball"))
		{
			gameObject.transform.position-=offset;
			life--;
		}
		if(life<=0&&!gameManager.BricksClear)
		{
			GameManager.GoToScene("BreakOut");
			gameManager.score=0;
		}
	}
}
