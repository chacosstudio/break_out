﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float speedX;
	Rigidbody2D rb;
	void Start () {
		rb=GetComponent<Rigidbody2D>();
	}
	
	void Update () {
		moveLeftOrRight();
	}
	float LeftOrRight(){
		return Input.GetAxis("Horizontal");
	}
	void moveLeftOrRight(){
		rb.velocity=LeftOrRight()*new Vector2(speedX,0);
	}
}
