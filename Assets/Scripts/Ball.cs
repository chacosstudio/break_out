﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ball : MonoBehaviour
{

    public Text scoreText;

    Rigidbody2D rb2;
    CircleCollider2D ballCircleCollider2D;

    public float speedX;
    public float speedY;
    private int brickCount;

    GameManager gameManager;
    void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
    }
    void Start()
    {
        rb2 = GetComponent<Rigidbody2D>();
        ballCircleCollider2D = GetComponent<CircleCollider2D>();
		scoreText.text = "Score: " + gameManager.score;        
		Invoke("BallStart", 5);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            BallStart();
        }

    }
    void BallStart()
    {
        if (IsStop())
        {
            rb2.bodyType = RigidbodyType2D.Dynamic;
            ballCircleCollider2D.enabled = true;
            transform.SetParent(null);
            rb2.velocity = new Vector2(speedX, speedY);
        }
    }
    bool IsStop()
    {
        return rb2.velocity == Vector2.zero;
    }
    void OnCollisionEnter2D(Collision2D other)
    {
        LockSpeed();
        if (other.gameObject.CompareTag("Brick"))
        {
            gameManager.brickCount--;
            Debug.Log("Current bricks amount: " + gameManager.brickCount);
            gameManager.CheckBrickClear();
            other.gameObject.SetActive(false);
            gameManager.score += 10;
            scoreText.text = "Score: " + gameManager.score;
        }
    }
    void LockSpeed()
    {
        rb2.velocity = new Vector2(ResetSpeedX(), ResetSpeedY());
    }
    float ResetSpeedX()
    {
        float currentSpeedX = rb2.velocity.x;
        if (currentSpeedX < 0)
        {
            return -speedX;
        }
        else
        {
            return speedX;
        }
    }
    float ResetSpeedY()
    {
        float currentSpeedY = rb2.velocity.y;
        if (currentSpeedY < 0)
        {
            return -speedY;
        }
        else
        {
            return speedY;
        }
    }
}
