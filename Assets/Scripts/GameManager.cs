﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	private static string[] LEVELS = {"BreakOut", "Level2", "Level3"};
    public int brickCount;
    private GameObject nextLevelBotton;
    static GameManager instance;
    public int score;

    public static void ReloadCurrentScene()
    {
        Scene current = SceneManager.GetActiveScene();
        SceneManager.LoadScene(current.name);
    }
    public bool BricksClear
    {
        get
        {
            if (brickCount <= 0)
            {
                return true;
            }
            return false;
        }
    }

    public void CheckBrickClear()
    {
        if (BricksClear)
        {
            ShowNextLevelBotton();
        }
    }
    public static void GoToScene(string level)
    {
        SceneManager.LoadScene(level);
        Debug.Log("Enter");
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            // The other scene which are loaded later should find the new one in their own Scene.
            SceneManager.sceneLoaded += (_, __) => SetupLevel();
            DontDestroyOnLoad(this);
            score = 0;
        }
        else if (this != instance)
        {
            //		string sceneName = SceneManager.GetActiveScene().name;
            //		Debug.Log("刪除場景"+sceneName+"的"+name);
            Destroy(this.gameObject);
        }
    }

    private void SetupLevel()
    {
		brickCount = GameObject.FindGameObjectsWithTag("Brick").Length;
		if (nextLevelBotton != null) {
			DestroyObject(nextLevelBotton);
		}
        nextLevelBotton = GameObject.FindGameObjectWithTag("NextLevelBotton");
		int currentLevel = Array.IndexOf(LEVELS, SceneManager.GetActiveScene().name);
		int nextLevel = currentLevel + 1;
		if (nextLevel < LEVELS.Length) {
			nextLevelBotton.GetComponent<Button>().onClick.AddListener(() => GoToScene(LEVELS[nextLevel]));
		}
		else {
			var button = nextLevelBotton.GetComponent<Button>();
			button.onClick.AddListener(() => {
				GoToScene(LEVELS[0]);
				score = 0;
			});
			var text = button.GetComponentInChildren<Text>();
			text.text = "Restart";
			// TODO: Show something?
		}
//		Debug.Log("next level botton = " + nextLevelBotton);
        nextLevelBotton.SetActive(false);
		
    }

    // Update is called once per frame
    // void Update()
    // {
	// 	brickCount = GameObject.FindGameObjectsWithTag("Brick").Length;
	// 	if (brickCount == 0) {
	// 		ShowNextLevelBotton();
	// 	}
    // }
    void ShowNextLevelBotton()
    {
        nextLevelBotton.SetActive(true);
    }
}
